[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/gitlab-de/tanukigotchi)

# Tanukigotchi

## Getting Started

### Developement

- Run `npm install`
- Set up your `.env` file - copy `.env.sample` and fill in the correct values
- `npm run dev`

### Build & Run in Docker
* Build with `docker build -t tanukigotchi .`
* Run with `docker run -p 5000:5000 -d --env-file .env tanukigotchi:latest`

## Database

Uses Postgres SQL 12, production hosted in Google Cloud SQL.

### Initalize

To initalize a new databse, run this command, replacing the user name and IP with the correct values

```
psql -h 35.194.13.101 -U postgres -f ./db/create.sql
```
