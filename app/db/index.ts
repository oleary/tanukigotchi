import { Sequelize } from 'sequelize';
import { logger } from '../lib/logger';
// import { Sequelize, Model, DataTypes } from 'sequelize';

logger.debug(`CLOUD_SQL_IP: ${process.env.CLOUD_SQL_IP}`);
logger.debug(`CLOUD_SQL_PWD: ${process.env.CLOUD_SQL_PWD}`);
logger.debug(`DATABASE_URL: ${process.env.DATABASE_URL}`);

// 'postgres://user:pass@example.com:5432/dbname'
const dbString = `postgres://postgres:${process.env.CLOUD_SQL_PWD || ''}@${
  process.env.CLOUD_SQL_IP || '127.0.0.1'
}:5432/postgres`;

// Here "DATABASE_URL" is the in-cluster database provided by Auto DevOps
// alternatively, if not specified we fall back on the CLOUD SQL path or local
const sequelize = new Sequelize(process.env.DATABASE_URL || dbString, {
  dialect: 'postgres',
  dialectOptions: {
    // Your pg options here
  },
});

export default sequelize;
