export const tanukiAdjectives: string[] = [
  'legendary',
  'mythical',
  'fabled',
  'folkloric',
];

export const tanukiFacts: string[] = [
  'reputed to be mischievous and jolly',
  'a master of disguise and shapeshifting',
  'somewhat gullible and absentminded',
  'a common theme in Japanese art',
  'a great collaborator',
];

export function getTanukiFact(): string {
  return tanukiFacts[Math.floor(Math.random() * tanukiFacts.length)];
}

export function getTanukiAdjective(): string {
  return tanukiAdjectives[Math.floor(Math.random() * tanukiAdjectives.length)];
}
