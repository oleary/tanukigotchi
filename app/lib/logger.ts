import { createLogger, format, transports } from 'winston';

const logConfig = {
  transports: [
    new transports.Console({
      format: format.combine(format.colorize(), format.simple()),
      level: process.env.LOG_LEVEL || 'debug',
    }),
    // TODO: Transport this JSON format somewhere else.
    // new transports.Console({
    //   format: format.combine(format.timestamp(), format.json()),
    // }),
  ],
};

export const logger = createLogger(logConfig);
