import { User, dbUser } from '../models/user';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export async function ensureUser(profile: unknown): Promise<any> {
  const userProfile: User = profile as User;
  return new Promise(async (resolve, reject) => {
    if (!userProfile.email) {
      reject('No email provided');
    } else {
      try {
        const u = await dbUser.findOrCreate({
          where: {
            email: userProfile.email,
          },
          defaults: {
            email: userProfile.email,
            nickname: userProfile.nickname || '',
            name: userProfile.name || '',
            picture: userProfile.picture || '',
            initialized: false,
          },
        });
        resolve(u);
      } catch (error) {
        console.error(error);
        reject(error);
      }
    }
  });
}
