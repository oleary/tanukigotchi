/* eslint-disable @typescript-eslint/no-var-requires */
const pkg = require('../../package.json');

export function getPackageVersion(): string {
  return pkg.version || 'UNK';
}
