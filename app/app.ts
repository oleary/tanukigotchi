require('dotenv').config(); // eslint-disable-line

import express = require('express');
import morgan = require('morgan');
import { join } from 'path';
import {
  auth,
  ConfigParams,
  requiresAuth,
  RequestContext,
} from 'express-openid-connect';

import { dbUser } from './models/user';

import { normalizePort } from './lib/server';
import { logger } from './lib/logger';
import * as routes from './routes';
import * as api from './api';

// Create a new express application instance
const app: express.Application = express();
app.use(morgan('dev'));

import handlebars from 'express-handlebars';
import { getPackageVersion } from './lib/version';

logger.debug(`AUTH0_SALT ${process.env.AUTH0_SALT}`);
logger.debug(`BASE_URL ${process.env.BASE_URL}`);

const config: ConfigParams = {
  authRequired: false,
  auth0Logout: true,
  secret: process.env.AUTH0_SALT || 'iamsaltythisdidntwork!@#$!1',
  baseURL: process.env.BASE_URL || 'http://localhost:5000',
  clientID: 'IRgXS4SYDvvehMH38YQhYEy6IdTyyZXp',
  issuerBaseURL: 'https://gitlab-de.us.auth0.com',
};

app.use(auth(config));

app.set('view engine', 'handlebars');
app.engine(
  'handlebars',
  handlebars({
    layoutsDir: './views/layouts',
    defaultLayout: 'main',
    helpers: {
      vuejs: function(options: any) {
        return options.fn();
      }
    }
  })
);

app.use(express.static('public'));

app.use(function (req, res, next) {
  const context: RequestContext = req.oidc;
  res.locals.auth = {
    isLoggedIn: context.isAuthenticated(),
    user: context.user,
  };
  res.locals.pkgversion = getPackageVersion();
  next();
});

// Set up routes from controllers
app.get('/', routes.homepage);
app.get('/about', routes.about);
app.get('/profile', requiresAuth(), routes.profile);

app.get('/app', requiresAuth(), routes.app.apphome);

//app.get('/api/now', requiresAuth(), api.now);

// Auth testings
app.get('/auth', (req, res) => {
  res.send(req.oidc.isAuthenticated() ? 'Logged in' : 'Logged out');
});

// Endpoint to serve the configuration file
app.get('/auth_config.json', (req, res) => {
  res.sendFile(join(__dirname, 'auth_config.json'));
});

const port = normalizePort(process.env.PORT || '5000');
app.listen(port, async () => {
  try {
    await dbUser.sync();
  } catch (error) {
    logger.error('Unable to sync databse');
    logger.error(error);
  }
  logger.info(`Tanukigotchi listening on port ${port}`);
});
