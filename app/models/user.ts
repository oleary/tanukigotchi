import { Model, DataTypes } from 'sequelize';

import sequelize from '../db';

export interface User extends Model {
  id: number;
  email: string;
  nickname: string;
  name: string;
  picture: string;
  initialized: boolean;
}

export const dbUser = sequelize.define<User>("user", {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER,
  },
  email: {
    type: DataTypes.STRING,
  },
  nickname: {
    type: DataTypes.STRING,
  },
  name: {
    type: DataTypes.STRING,
  },
  picture: {
    type: DataTypes.STRING,
  },
  initialized: {
    type: DataTypes.BOOLEAN,
  }
});
