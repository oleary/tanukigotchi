import { Request, Response } from 'express';
import * as db from '../db';

function apiError(err: any, res: Response) {
  console.error(err);
  res.status(500).send(err);
}

// export const now = async (req: Request, res: Response) => {
//   if (!req.oidc.isAuthenticated()) res.redirect('/404');

//   db.asyncQuery('SELECT NOW()')
//     .then((results) => {
//       res.json(results.rows);
//     })
//     .catch((e) => apiError(e, res));
// };
